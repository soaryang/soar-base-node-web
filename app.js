var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer=require('multer');
var session=require('express-session')


//业务controller
var routes = require('./routes/index');
var users = require('./routes/users');
var danmu = require('./routes/danmu');

//生成一个express实例 app。
var app = express();

app.use(session({
  secret: 'secret',
  cookie:{
    maxAge: 1000*60*30
  }
}));

// view engine setup
//设置 views 文件夹为存放视图文件的目录, 即存放模板文件的地方,__dirname 为全局变量,存储当前正在执行的脚本所在的目录。
app.set('views', path.join(__dirname, 'views'));

//console.log('__dirname------->'+__dirname);

//设置视图模板引擎为 html
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');
//设置视图模板引擎为 ejs。
//app.set('view engine', 'ejs');

//设置/public/favicon.ico为favicon图标。
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//加载日志中间件。
app.use(logger('dev'));
//加载解析json的中间件。
app.use(bodyParser.json());
//加载解析urlencoded请求体的中间件
app.use(bodyParser.urlencoded({ extended: false }));
//加载解析cookie的中间件。
app.use(cookieParser());
//设置public文件夹为存放静态文件的目录。
app.use(express.static(path.join(__dirname, 'public')));

//路由控制器。
app.use('/', routes);
app.use('/login', routes);
//控制用户路由
app.use('/users', users);
//弹幕
app.use('/danmu',danmu);

//跳转到404
app.use(function(req, res, next) {
  /*var err = new Error('Not Found');
  err.status = 404;
  next(err);*/
  res.render('404.html', {title: 'No Found'})
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
